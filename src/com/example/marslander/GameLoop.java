package com.example.marslander;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameLoop extends SurfaceView implements Runnable, 
             SurfaceHolder.Callback {
	
	public static double INITIAL_TIME = 3.5;
	static final int REFRESH_RATE = 20;
	static final int GRAVITY = 1;
	private static int FUEL = 450;
	public int SCORE = 0;
	private static int TIME = 400;
    public boolean start = false;
    
	Canvas canvas = null;
    Thread main;
    
	Paint paint = new Paint();
	Paint txtPaint = new Paint();
	Bitmap land = BitmapFactory.decodeResource(getResources(), R.drawable.mars);
	Bitmap ship = BitmapFactory.decodeResource(getResources(), R.drawable.craftmain);
	Bitmap background = BitmapFactory.decodeResource(getResources(), R.drawable.space);
	BitmapShader shader = new BitmapShader(land, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
	BitmapShader spaceShader = new BitmapShader(background, Shader.TileMode.MIRROR, Shader.TileMode.MIRROR);
	Bitmap mainFlame = BitmapFactory.decodeResource(getResources(), R.drawable.main_flame);
	Bitmap thruster = BitmapFactory.decodeResource(getResources(), R.drawable.thruster);
	Bitmap explosion = BitmapFactory.decodeResource(getResources(), R.drawable.explosion);
	
	int[] xcor = new int[54];
	int[] ycor = new int[54];
	
	
	int width = background.getWidth();
	int height = background.getHeight();
	
	boolean upPressed = false;
	boolean leftPressed = false;
	boolean rightPressed = false;
	boolean gameover = false;
	
	float x, y;
	double t = INITIAL_TIME;
	Path path;

	public GameLoop(Context context) {
		super(context);
		
		initiate();
		
	}
	
	public GameLoop(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		initiate();
	}

	public GameLoop(Context context, AttributeSet attrs) {
		super(context, attrs);

		initiate();
	}
	
	public void initiate()
	{
		path = new Path();	
		
		landShapes(width,height);
		
		for (int i = 0; i < xcor.length; i++) 
		{
			path.lineTo(xcor[i], ycor[i]);
		}

		getHolder().addCallback(this);
	}
	
	private void landShapes(int width, int height)
	{	
		xcor[0] = 0;
		xcor[51] = width;
		xcor[52] = width;
		xcor[53] = 0;
		for(int i=1; i<xcor.length-3; i++)
		{
			xcor[i] = i * (width / 25);
		}
		for(int i=0; i<ycor.length-2; i++)
		{
			double random = (0.2 + Math.random()*0.15)*height;
			ycor[i] = (int) random;
		}
		ycor[53]= ycor[52] =height;
		
		int landingPoint = (int) (Math.random()*30);
		for(int i=1; i<4; i++)
		{
		    ycor[landingPoint+i]=ycor[landingPoint];
		}
	}
	
	public int[] getXcor(){
		return this.xcor;
	}
	
	public int[]getYcor(){
		return this.ycor;
	}
	
	public boolean contains(int[] xcor, int[] ycor, double x0, double y0) {
		int crossings = 0;

		for (int i = 0; i < xcor.length - 1; i++) {
			int x1 = xcor[i];
			int x2 = xcor[i + 1];

			int y1 = ycor[i];
			int y2 = ycor[i + 1];

			int dy = y2 - y1;
			int dx = x2 - x1;

			double slope = 0;
			if (dx != 0) {
				slope = (double) dy / dx;
			}

			boolean cond1 = (x1 <= x0) && (x0 < x2); // is it in the range?
			boolean cond2 = (x2 <= x0) && (x0 < x1); // is it in the reverse
														// range?
			boolean above = (y0 < slope * (x0 - x1) + y1); // point slope y - y1

			if ((cond1 || cond2) && above) {
				crossings++;
			}
		}
		return (crossings % 2 != 0); // even or odd
	}
	
	public void run() {
		while(true)
		{
			while (!gameover)
			{	
				if (start)
				{
					SurfaceHolder holder = getHolder();
					synchronized (holder) {
						
						canvas = holder.lockCanvas();
						paint.setShader(spaceShader);
						canvas.drawPaint(paint);
						
						paint.setShader(shader);
						canvas.drawPath(path, paint);
						
					
						y = (int) y + (int) ((0.5 * (GRAVITY * t * t)));
						
						t = t + 0.001; // increment the parameter for synthetic time by a small amount
						
						txtPaint.setColor(Color.WHITE);
						txtPaint.setTextSize(40);
						canvas.drawText("Fuel  : " + IntToStringFuel(FUEL), 25, 50, txtPaint);
						txtPaint.setColor(Color.WHITE);
						txtPaint.setTextSize(40);
						canvas.drawText("Time  : " + IntToStringTime(TIME), 25, 100, txtPaint);
						txtPaint.setTextSize(40);
						canvas.drawText("Score  : " + IntToStringScore(SCORE), 25, 150, txtPaint);
						
						boolean bottomLeft = contains(xcor, ycor, x-25, y+25);
						boolean bottomRight = contains(xcor, ycor, x+25, y+25);
						boolean bottom = contains(xcor, ycor, x, y+25);
						
						
						if (bottomLeft && bottomRight)
						{	
							txtPaint.setColor(Color.RED);
							txtPaint.setTextSize(60);
							canvas.drawText("YOU WIN", 380, 500, txtPaint);
							canvas.drawBitmap(ship, x-25, y-55, null);
							
							if (TIME >= 120)
							{
								SCORE = SCORE + 50;
							}
							else if (TIME < 120 || TIME > 70)
							{
								SCORE = SCORE + 30;
							}
							else if (TIME <= 70 || TIME > 30)
							{
								SCORE = SCORE + 10;
							}
							
							SCORE = SCORE + 50;
	                        t = INITIAL_TIME; // reset the time variable
							gameover = true;
						}
						else if (bottomLeft || bottomRight || bottom)
						{
							if (SCORE <= 50)
							{
							txtPaint.setColor(Color.RED);
							txtPaint.setTextSize(60);
							canvas.drawText("GAME OVER", 380, 500, txtPaint);
							canvas.drawBitmap(explosion, x-120, y-140, null);
							SCORE = SCORE - 50;
							t = INITIAL_TIME; // reset the time variable
							gameover = true;
							}
							else
							{
								txtPaint.setColor(Color.YELLOW);
								txtPaint.setTextSize(60);
								canvas.drawText("Try again", 380, 500, txtPaint);
								canvas.drawBitmap(explosion, x-120, y-140, null);
								SCORE = SCORE - 50;							
								t = INITIAL_TIME; // reset the time variable
								gameover = true;
							}
							
						}
						else
						{	 				
							canvas.drawBitmap(ship, x-25, y-25, null);
							
							
						}				
					}
					
					if (FUEL > 0)
					{
						if (rightPressed)
						{	
							if (x < 0)
							{
								x = width;
							}
							x = x-5;
							canvas.drawBitmap(thruster, x+60, y+60, null);						
							FUEL--;
							
						}
						if (leftPressed)
						{
							
							if (x > width)
							{
								x = 0;
							}
							x = x+5;
							canvas.drawBitmap(thruster, x-30, y+60, null);
							FUEL--;
							
						}
						if (upPressed)
						{
							y = y-10;
							canvas.drawBitmap(mainFlame, x+5, y+60, null);
							FUEL--;			
						}
					}
		
					try {
						Thread.sleep(REFRESH_RATE);
					} catch (Exception e) 
					{
						e.printStackTrace();
					}
		
					finally
					{
						if (canvas != null)
						{
							holder.unlockCanvasAndPost(canvas);
						}
					}
					if (TIME > 0)
					{
					  TIME--;
					}
					start = true;
				}
			}
		}
		
	}

	private int IntToStringFuel(int fuel) {
		// TODO Auto-generated method stub
		return FUEL;
	}
	
	private int IntToStringTime(int time) {
		// TODO Auto-generated method stub
		return TIME;
	}
	
	private int IntToStringScore(int score) {
		// TODO Auto-generated method stub
		return SCORE;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		width = w;	
		x = width / 2;
	}
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	public void surfaceCreated(SurfaceHolder holder) {
		main = new Thread(this);
		if (main != null)
		{
			main.start();
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		while (retry)
		{
			try
			{
				main.join();
				retry = false;
			}
			catch (InterruptedException e)
			{
				// try again shutting down the thread
			}
		}
	}
	
	public void reset()
	{	
		gameover = false;
		x = width / 2;
		y = 0;
		t = INITIAL_TIME;
		FUEL = 400;
		TIME = 300;
		SCORE = 0;
		initiate();
	}
	
	public void Continue()
	{
		gameover = false;
		x = width / 2;
		y = 0;
		t = INITIAL_TIME;
		FUEL = 400;
		TIME = 300;
		initiate();
	}

}
