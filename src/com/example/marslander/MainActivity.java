package com.example.marslander;

import java.io.IOException;

import android.support.v7.app.ActionBarActivity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements OnTouchListener, SensorEventListener{
	private GameLoop gameLoop;
	private ImageView btnLeft;
	private ImageView btnRight;
	private ImageView btnUp;
	private ImageView btnStart;
	private ImageView logo;
	private TextView title;
	private boolean accelerometer = false;
	boolean btnRightPressed = false;
	boolean btnLeftPressed = false;
	boolean btnUpPressed = false;
	private MediaPlayer mediaPlayer;
	private boolean release = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);	
		gameLoop = (GameLoop)findViewById(R.id.gameLoop);
		
		btnLeft = (ImageView)findViewById(R.id.imgLeft);
		btnLeft.setOnTouchListener(this);
		btnRight = (ImageView)findViewById(R.id.imgRight);
		btnRight.setOnTouchListener(this);
		btnUp = (ImageView)findViewById(R.id.imgUp);
		btnUp.setOnTouchListener(this);
		btnStart = (ImageView)findViewById(R.id.imgStart);
		btnStart.setOnTouchListener(this);
		logo = (ImageView)findViewById(R.id.imgGameLogo);
		title = (TextView)findViewById(R.id.txtVgameTitle);
		
		SensorManager sensorManager=(SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        
        mediaPlayer = MediaPlayer.create(this, R.raw.sound);
        sound();
	}
	 
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case R.id.action_continue:
			if (gameLoop.gameover == true)
			{
				if (gameLoop.SCORE > 0)
				{
			       gameLoop.Continue();
				}
			}
			break;
		case R.id.action_restart:
			gameLoop.reset();
			break;
		case R.id.action_sound:
			soundControl();
			break;
		case R.id.action_Accelerometer:
			accelerometer();
			break;
		case R.id.action_exit:
        	System.exit(0);
        	break;
		default:
            return super.onOptionsItemSelected(item);
		}
		return true;
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {

	    switch (event.getAction() & MotionEvent.ACTION_MASK) {

	    case MotionEvent.ACTION_DOWN:
	        if (v.getId() == btnStart.getId())
	        {
	        	gameLoop.start = true;
		        btnStart.setVisibility(View.INVISIBLE);
		        logo.setVisibility(View.INVISIBLE);
		        title.setVisibility(View.INVISIBLE);       
	        }
	        if (v.getId() == btnLeft.getId())
	        {
	        	gameLoop.leftPressed = true;
	        }
	        if (v.getId() == btnRight.getId())
	        {
	        	gameLoop.rightPressed = true;
	        }  
	        if (v.getId() == btnUp.getId())
	        {
	        	gameLoop.upPressed = true;
	        }
	        break;
	    case MotionEvent.ACTION_UP:
	    	gameLoop.leftPressed = false;
	    	gameLoop.rightPressed = false;
	    	gameLoop.upPressed = false;
	        break;
	    }
	    

	    return true;
	    
	}
	
	@Override
	   public void onSensorChanged(SensorEvent event) {
	        if (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
	        	if (accelerometer)
		         {
		        	
		            float ax =event.values[0];
		            float ay =event.values[1];
		            
		            if(ax > 2)
		            {
		            	gameLoop.leftPressed = true;
		            }
		            else if (ax < -2)
		            {
		            	gameLoop.rightPressed = true;
		            }
		            else if (ay < -1)
		            {
		            	gameLoop.upPressed = true;
		            }
		            else
		            {
		            	gameLoop.leftPressed = false;
		    	    	gameLoop.rightPressed = false;
		    	    	gameLoop.upPressed = false;
		            }
		        }
	        }
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
	
	private void accelerometer()
	{
		if (accelerometer)
		{
			accelerometer = false;			
		}
		else
		{
			accelerometer = true;	
		}
	}
	
	private void soundControl()
	{
		if (mediaPlayer.isPlaying())
		{
			mediaPlayer.pause();
		}
		else
		{
			mediaPlayer.start();
		}
	}
	
	private void sound()
	{
		try 
		{
			mediaPlayer.setLooping(true);
			mediaPlayer.prepare();
			
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mediaPlayer.start();	
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(release){
			mediaPlayer = MediaPlayer.create(this, R.raw.sound);
			try {
				mediaPlayer.setLooping(true);
				mediaPlayer.prepare();
				
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
			mediaPlayer.start();
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mediaPlayer.release();
		release=true;
	}
	
}
